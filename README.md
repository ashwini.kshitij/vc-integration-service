# vc-integration-service

vc-integration-service is a Go project that provides an integration service for various video conferencing platforms. It exposes functionalities such as health checks, consent sending, enabling/disabling recording, and reconciliation.

## Features

- Health Check: Checks if the service is healthy.
- Consent Sending: Sends consent for a particular action.
- Enable/Disable Recording: Allows enabling or disabling of meeting recording.
- Reconciliation: Reconciles the state of the service.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Go (version X.X.X)
- GoLand IDE (optional)

### Installing

Clone the repository:

```bash
git clone https://gitlab.com/ashwini.kshitij/vc-integration-service.git