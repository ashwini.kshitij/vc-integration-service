package scripts

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"
	"vc-integration-service/configs"
	"vc-integration-service/utils"
)

type Participant struct {
	Name  string `json:"name"`
	Email string `json:"email"`
	Type  string `json:"type"`
}

type MovieReadyS3Info struct {
	SrcBucket string `json:"src_bucket"`
	SrcKey    string `json:"src_key"`
}

type Meeting struct {
	Date             string           `json:"date"`
	Title            string           `json:"title"`
	Participants     []Participant    `json:"participants"`
	ExternalID       string           `json:"external_id"`
	Duration         float64          `json:"duration"`
	RecType          string           `json:"rec_type"`
	MovieReadyS3Info MovieReadyS3Info `json:"movie_ready_s3_info"`
}

func MigrateToCallAI(date time.Time, externalID string, duration float64, mediaUrl string) {

	log.Printf("Migrating call to CallAI for date: %v, externalID: %v, duration: %v, mediaUrl: %v", date, externalID, duration, mediaUrl)
	meeting_title := "Recall-Demo-" + time.Now().Format("20060102150405")
	key := meeting_title + ".mp4"
	err := utils.UploadToS3(mediaUrl, key, configs.GetConfig().AWS.Bucket)
	if err != nil {
		log.Printf("Error uploading to S3: %v", err)
		return
	}

	var meeting = Meeting{
		Date:       date.Format("2006-01-02"),
		Title:      meeting_title,
		ExternalID: externalID,
		Participants: []Participant{
			{
				Name:  "Sharaj Rewoo",
				Email: "sharaj.rewoo@mindtickle.com",
				Type:  "USER",
			},
			{
				Name:  "Jane Doe",
				Email: "jane.doe@example.com",
				Type:  "PROSPECT",
			},
		},
		Duration: duration,
		RecType:  "MOVIE",
		MovieReadyS3Info: MovieReadyS3Info{
			SrcBucket: configs.GetConfig().AWS.Bucket,
			SrcKey:    key,
		},
	}
	sendMeetingToCallAI(meeting)
}

func sendMeetingToCallAI(meeting Meeting) {
	jsonData, err := json.Marshal(meeting)
	if err != nil {
		log.Printf("Error marshalling meeting data: %v", err)
		return
	}
	log.Printf("Meeting data: %v", string(jsonData))

	req, err := http.NewRequest("POST", configs.GetConfig().Migrate.API, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Printf("Error creating request: %v", err)
		return
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Error sending request: %v", err)
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Printf("Error closing response body: %v", err)
			return
		}
	}(resp.Body)

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Printf("Error reading response body: %v", err)
		return
	}
	log.Printf("Response: %v", string(respBody))
}
