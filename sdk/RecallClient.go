package sdk

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/pkg/errors"
	"io"
	"sync"
	"time"
	"vc-integration-service/configs"
	"vc-integration-service/constants"
	"vc-integration-service/dto/recall"
)

type RecallService struct {
	Client *retryablehttp.Client
}

var (
	once                  sync.Once
	recallServiceInstance *RecallService
)

// TODO: Need to write retry tests for these APIs through mocking
func RecallClient() *RecallService {
	once.Do(func() {
		client := retryablehttp.NewClient()
		client.RetryMax = 2
		client.RetryWaitMin = 3 * time.Second
		client.RetryWaitMax = 30 * time.Second

		recallServiceInstance = &RecallService{
			Client: client,
		}
	})

	return recallServiceInstance
}

func (s *RecallService) doRequest(method, url string, body []byte) ([]byte, error) {
	req, err := retryablehttp.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return nil, errors.Wrap(err, "creating request failed")
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("%s", configs.GetConfig().Recall.APIKey))

	resp, err := s.Client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "request failed")
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Println("Error closing response body")
		}
	}(resp.Body)

	// Check if the status code is in the 4xx range
	if resp.StatusCode >= 400 && resp.StatusCode < 500 {
		return nil, errors.Errorf("request failed with status code %d", resp.StatusCode)
	}

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "reading response body failed")
	}

	return respBody, nil
}

func (s *RecallService) CreateBot(request recall.CreateBotRequest) (recall.BotResponse, error) {
	requestBody, err := json.Marshal(request)
	if err != nil {
		return recall.BotResponse{}, errors.Wrap(err, "marshalling request failed")
	}
	url := fmt.Sprintf(constants.RecallAPIs.CreateBot.Path, configs.GetConfig().Recall.Host)
	respBody, err := s.doRequest(constants.RecallAPIs.CreateBot.RequestType, url, requestBody)

	if err != nil {
		return recall.BotResponse{}, err
	}

	var botResponse recall.BotResponse
	err = json.Unmarshal(respBody, &botResponse)
	if err != nil {
		return recall.BotResponse{}, errors.Wrap(err, "unmarshalling response failed")
	}

	err = constants.ValidateJSON(botResponse)
	if err != nil {
		return recall.BotResponse{}, err
	}

	return botResponse, nil
}

func (s *RecallService) DeleteScheduledBot(botID string) (string, error) {
	url := fmt.Sprintf(constants.RecallAPIs.DeleteBot.Path, configs.GetConfig().Recall.Host, botID)
	respBody, err := s.doRequest(constants.RecallAPIs.DeleteBot.RequestType, url, nil)
	if err != nil {
		return "", err
	}
	return string(respBody), nil
}

func (s *RecallService) RetrieveBot(botID string) (recall.BotResponse, error) {
	url := fmt.Sprintf(constants.RecallAPIs.RetrieveBot.Path, configs.GetConfig().Recall.Host, botID)
	respBody, err := s.doRequest(constants.RecallAPIs.RetrieveBot.RequestType, url, nil)
	if err != nil {
		return recall.BotResponse{}, err
	}

	var botResponse recall.BotResponse
	err = json.Unmarshal(respBody, &botResponse)
	if err != nil {
		return recall.BotResponse{}, errors.Wrap(err, "unmarshalling response failed")
	}

	err = constants.ValidateJSON(botResponse)
	if err != nil {
		return recall.BotResponse{}, err
	}

	return botResponse, nil
}

func (s *RecallService) GetSpeakerChange(botID string) (recall.SpeakerChangeResponse, error) {
	url := fmt.Sprintf(constants.RecallAPIs.GetSpeakerChange.Path, configs.GetConfig().Recall.Host, botID)
	respBody, err := s.doRequest(constants.RecallAPIs.GetSpeakerChange.RequestType, url, nil)
	if err != nil {
		return recall.SpeakerChangeResponse{}, err
	}

	var speakerChangeResponse recall.SpeakerChangeResponse
	err = json.Unmarshal(respBody, &speakerChangeResponse)
	if err != nil {
		return recall.SpeakerChangeResponse{}, errors.Wrap(err, "unmarshalling response failed")
	}

	err = constants.ValidateJSON(speakerChangeResponse)
	if err != nil {
		return recall.SpeakerChangeResponse{}, err
	}

	return speakerChangeResponse, nil
}
