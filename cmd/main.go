package main

import (
	"vc-integration-service/db_models"
	"vc-integration-service/routes"
)

func main() {
	//Init DB
	db_models.GetDB()

	// Setup routes
	e := routes.InitRoutes()

	// Start the server
	err := e.Start(":8080")
	if err != nil {
		// print the error and exit
		panic(err)
	}
}
