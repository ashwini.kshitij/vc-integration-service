package utils

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"io"
	"log"
	"net/http"
	"sync"
	"vc-integration-service/configs"
)

type AWSSession struct {
	Uploader *s3manager.Uploader
}

var instance *AWSSession
var once sync.Once

func GetAWSSession() *AWSSession {
	once.Do(func() {
		config := configs.GetConfig()
		sess, err := session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewStaticCredentials(config.AWS.AccessKey, config.AWS.SecretKey, config.AWS.SessionToken),
		})

		if err != nil {
			panic(err)
		}

		instance = &AWSSession{
			Uploader: s3manager.NewUploader(sess),
		}
	})

	return instance
}

func UploadToS3(fileURL string, key string, bucketName string) error {

	awsSession := GetAWSSession()
	resp, err := http.Get(fileURL)
	if err != nil {
		log.Printf("Error downloading file: %v", err)
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Printf("Error closing response body: %v", err)
		}
	}(resp.Body)

	_, err = awsSession.Uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(key),
		Body:   resp.Body,
	})

	if err != nil {
		log.Printf("Error uploading file to S3: %v", err)
		return err
	}
	log.Printf("File uploaded to S3: key: %v, bucket: %v", key, bucketName)
	return nil
}
