package utils

import (
	"fmt"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

func GetMediaFileDuration(url string) (time.Duration, error) {
	// Prepare the ffmpeg command to get the duration
	cmd := exec.Command("ffmpeg", "-i", url, "-show_entries", "format=duration", "-v", "quiet", "-of", "csv=p=0")

	// Run the command and capture the output and error
	output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(string(output)) // Print the error output
		return 0, err
	}

	// The output will be a string like "123.456\n"
	// We need to parse that into a duration
	durationStr := strings.TrimSpace(string(output)) // Remove the newline
	durationSecs, err := strconv.ParseFloat(durationStr, 64)
	if err != nil {
		return 0, err
	}

	// Convert seconds to a time.Duration and return
	return time.Duration(durationSecs * float64(time.Second)), nil
}
