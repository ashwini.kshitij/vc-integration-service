package utils

import (
	"log"
	"testing"
)

func TestGetMediaFileDuration(t *testing.T) {
	// URL of the media file to download
	url := "https://call-ai-mt-integration-media.s3.ap-south-1.amazonaws.com/video-cc4c20d9-04ef-467b-920a-d5476cc3236f.mp4?response-content-disposition=inline&X-Amz-Security-Token=IQoJb3JpZ2luX2VjELj%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCmFwLXNvdXRoLTEiSDBGAiEAkY4aSdzIc7%2FOtece2OlIcxWWzHZpJGnPg12RBwABd08CIQCxooRUHpSGN3%2FZ4UvNAcQC6%2FnrkV6xUu0bGOoUZC8Ndir%2FAwgREAMaDDg0NTkyNTgwODIzMiIMOZ0gj%2BFTQe6pCxOFKtwDOhFRt0DyxZiPtkniTqqwT0oQqLiwRYcyZRlfANBwnHm697FTC126X7h17ygug5nCtpAGKcczTDxqODH81DTDUy4p8AyX7cVGXVnKxlEsbnG2HZ8yNoPHOuf%2FbBFGi5QTrLceverM9i2tsrunTWRpslAj%2BRJCeG8Qf%2B2A1pagXmuoFW%2B1MCfBANjNs2%2FjrYwQiRSbhZd%2FY2tK5wRKqmRk55xmg9XU9WUFBRfggfdzGQ6kx8GHxTeIxDf0vVJsrfXSmjmpO27OPoHLuJ4AsDVUjPnR4Ax11O38bgzpRm4sBfQKRLFKXNT3Pff73Hb1wrkfNWLXwYBT6E7eFj6gYlZ9fv5C%2FTveTto8gCrkhqkgMpVSrjP6hLxlU0xeX8Urt0wfZfroffS91bpR7yJZHlcfKJbhPIirpNDcfXpeXNsWXBgwIlmyqY014bPBFcnn4rHTYynnnHs%2BZC8YQdMRQHYh5TjR5wuuZ8TfNhUyROl0JueZTdxF7OXnM0KRaz5gY6OQArndG2qH493WpM%2FNj56caZ4Wj2RcdmJovHG4MjMnRvy5xNlcFgYPke1vI7mArTWLDPO1Dfajho7x%2FEQty2h1oyzVtssUKXBlUcWT%2BGOSn4QSx9rpv%2B8ItU%2FCXlowkae9sQY6kwLsQRiHGPgfVhqDA64O5uWsGNzRqy4gFFcLHaDL5VN1%2B8sRFx1eEJ5aOlrjRTO%2FDVnbds6xoGsApClql%2FRvsGMBpVjFJX7GYUR0VDKHyNIR%2B5x6JoUPxwTyC2CFnaonqa35%2BL%2BQlmhjTu7OhxsaLjfZ8jI7M5fAInyZJ6bxty6KFZhpc61FqXAVZwpnmvzm4U861c0cmn21Lh1zpOjj31VipPHfYwaPO9G0hGcLmSJZGmFUbzh4m2qgLw1mb%2FWiN7NKXUFM1aCSn6MGS9XX7Byiraii6sia8BQ0Yi3xsrgdN8WureoVI%2F6nvVAf4aNP0arREosK1%2FmDxYB5Ianug%2Fwk3BkpR4CQ043oV8xy8clssSBMJg%3D%3D&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20240429T125952Z&X-Amz-SignedHeaders=host&X-Amz-Expires=43200&X-Amz-Credential=ASIA4J5I3PBUPMUGBO7Y%2F20240429%2Fap-south-1%2Fs3%2Faws4_request&X-Amz-Signature=d71065af08e763041f7394377e2e04ac121d0029ba9633a6047fde29d8249306"

	// Download the file
	duration, err := GetMediaFileDuration(url)
	if err != nil {
		t.Errorf("Error getting media file duration: %v", err)
	}
	log.Printf("Duration: %v", duration)
}
