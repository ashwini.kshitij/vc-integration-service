package configs

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"os"
	"sync"
)

type Config struct {
	DB struct {
		Type     string `yaml:"type"`
		Host     string `yaml:"host"`
		Port     int    `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		DBName   string `yaml:"dbname"`
	} `yaml:"db"`
	Kafka struct {
		Producer struct {
			Brokers []string `yaml:"brokers"`
			Topic   string   `yaml:"topic"`
		} `yaml:"producer"`
		Consumer struct {
			Brokers []string `yaml:"brokers"`
			Group   string   `yaml:"group"`
			Topics  []string `yaml:"topics"`
		} `yaml:"consumer"`
	} `yaml:"kafka"`
	Recall struct {
		Host                             string `yaml:"defaultHost"`
		APIKey                           string `yaml:"apiKey"`
		BotStatusChangeEventWebhookToken string `yaml:"botStatusChangeEventWebhookToken"`
	} `yaml:"recall"`
	AWS struct {
		Region       string `yaml:"region"`
		Bucket       string `yaml:"bucket"`
		AccessKey    string `yaml:"access_key"`
		SecretKey    string `yaml:"secret_key"`
		SessionToken string `yaml:"session_token"`
	} `yaml:"aws"`
	Migrate struct {
		API string `yaml:"api"`
	} `yaml:"migrate"`
}

var (
	cfg  Config
	once sync.Once
)

func GetConfig() Config {
	once.Do(func() {
		// Read config file
		file, err := os.ReadFile("config.yaml")
		if err != nil {
			fmt.Println("Error reading config file", err)
		}

		// Unmarshal the file content into config struct
		err = yaml.Unmarshal(file, &cfg)
		if err != nil {
			fmt.Println("Error parsing config file", err)
		}
	})

	return cfg
}
