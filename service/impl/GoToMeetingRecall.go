package impl

import (
	"errors"
	"fmt"
	"log"
	"time"
	"vc-integration-service/db_models"
	"vc-integration-service/dto/recall"
	"vc-integration-service/sdk"
)

// GoToMeetingRecall is a struct that will implement the RecallService interface
type GoToMeetingRecall struct{}

// IsHealthy checks if the service is healthy
func (r GoToMeetingRecall) IsHealthy() bool {
	fmt.Println("Checking if service is healthy...")
	// Implement your logic here
	return true
}

// SendConsent sends consent
func (r GoToMeetingRecall) SendConsent() error {
	return errors.New("operation not supported in this implementation")
}

// EnableRecording enables recording
func (r GoToMeetingRecall) EnableRecording(meeting *db_models.Meeting) error {

	// Use the singleton instance of RecallService
	recallService := sdk.RecallClient()

	// Prepare the CreateBotRequest
	createBotRequest := recall.CreateBotRequest{
		MeetingURL: meeting.MeetingURL,
		JoinAt:     meeting.ScheduledStartTime.Add(-1 * time.Minute).Format(time.RFC3339),
		BotName:    "VIS Bot", // replace with your bot name
	}

	// Call the CreateBot method
	bot, err := recallService.CreateBot(createBotRequest)
	if err != nil {
		log.Printf("Error creating bot: %v", err)
		return errors.New("error creating bot")
	}

	persistedBot, err := (&db_models.Bot{}).CreateBot(bot.ID, meeting.ID)
	if err != nil {
		log.Printf("Error persisting bot in DB: %v", err)
		return errors.New("error creating bot")
	}

	log.Printf("Bot created successfully: %v", persistedBot.ID)
	return nil
}

// DisableRecording disables recording
func (r GoToMeetingRecall) DisableRecording() error {
	fmt.Println("Disabling recording...")
	// Implement your logic here
	return nil
}

// Reconcile reconciles the service
func (r GoToMeetingRecall) Reconcile() error {
	fmt.Println("Reconciling service...")
	// Implement your logic here
	return nil
}
