package factory_test

import (
	"testing"
	"vc-integration-service/constants"
	"vc-integration-service/service/factory"
)

func TestGetExternalVCPlatform(t *testing.T) {
	// Pass test
	t.Run("valid clientID", func(t *testing.T) {
		_, err := factory.GetExternalVCPlatform(uint(constants.GoTo))
		if err != nil {
			t.Errorf("Expected no error, but got: %v", err)
		}
	})

	// Fail test
	t.Run("invalid clientID", func(t *testing.T) {
		_, err := factory.GetExternalVCPlatform(9999) // 9999 is an invalid clientID
		if err == nil {
			t.Errorf("Expected an error, but got none")
		}
	})
}
