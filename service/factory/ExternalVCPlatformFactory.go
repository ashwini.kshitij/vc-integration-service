package factory

import (
	_ "errors"
	"fmt"
	"vc-integration-service/constants"
	"vc-integration-service/service"
	"vc-integration-service/service/impl"
)

// ExternalVCPlatformFactory is a function that returns an instance of a type that implements the ExternalVCPlatform interface
type ExternalVCPlatformFactory func() service.ExternalVCPlatform

/*
GetExternalVCPlatform is a factory function that returns an instance of a type that implements the ExternalVCPlatform interface.
It takes a clientID as an argument, which is used to determine the type of ExternalVCPlatform to return.
*/
func GetExternalVCPlatform(clientID uint) (service.ExternalVCPlatform, error) {
	vcTool, ok := constants.ClientIDToVCTool[constants.ClientID(clientID)]
	if !ok {
		return nil, fmt.Errorf("VC Platform service not implemented for clientID: %d", clientID)
	}

	switch vcTool {
	case constants.GoTo:
		return &impl.GoToMeetingRecall{}, nil
	default:
		return nil, fmt.Errorf("VC Platform service not implemented for vcTool: %d", vcTool)
	}
}
