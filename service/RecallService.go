package service

import (
	"vc-integration-service/db_models"
)

type ExternalVCPlatform interface {
	IsHealthy() bool
	SendConsent() error
	EnableRecording(meeting *db_models.Meeting) error
	DisableRecording() error
	Reconcile() error
}
