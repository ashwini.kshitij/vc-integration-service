package db_models

import (
	"gorm.io/gorm"
	"time"
)

type Meeting struct {
	gorm.Model
	MeetingURL         string    `gorm:"type:varchar(255)"`
	EventID            string    `gorm:"type:varchar(255);unique"`
	RecordingStatus    string    `gorm:"type:varchar(255)"`
	ScheduledStartTime time.Time `gorm:""`
	ScheduledEndTime   time.Time `gorm:""`
}

// TODO: Wrap around multiple db operations in a transaction
func (m *Meeting) Create(MeetingURL string, EventID string, ScheduledStartTime time.Time, ScheduledEndTime time.Time) (*Meeting, error) {
	m.MeetingURL = MeetingURL
	m.EventID = EventID
	m.RecordingStatus = "INITIATED"
	m.ScheduledStartTime = ScheduledStartTime
	m.ScheduledEndTime = ScheduledEndTime

	db := GetDB().Create(m)
	if db.Error != nil {
		return nil, db.Error
	}
	//update meeting status
	err := Create(m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func (m *Meeting) UpdateRecordingStatus(RecordingStatus string) (*Meeting, error) {
	m.RecordingStatus = RecordingStatus
	db := GetDB().Save(m)
	if db.Error != nil {
		return nil, db.Error
	}
	err := Create(m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func GetMeetings(fields map[string]interface{}) ([]Meeting, error) {
	var meetings []Meeting
	db := GetDB().Model(&Meeting{})
	for key, value := range fields {
		db = db.Where(key+" = ?", value)
	}
	result := db.Find(&meetings)
	if result.Error != nil {
		return nil, result.Error
	}
	return meetings, nil
}
