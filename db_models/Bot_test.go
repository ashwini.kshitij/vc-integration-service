package db_models

/*
import (
	"testing"
	"time"
)

func TestCreateBot(t *testing.T) {
	bot := &Bot{}
	_, err := bot.CreateBot("test-bot-id", 1)
	if err != nil {
		t.Errorf("Failed to create bot: %v", err)
	}

	_, err = bot.CreateBot("test-bot-id", 1)
	if err == nil {
		t.Errorf("Expected error when creating bot with duplicate ID, got nil")
	}
}

func TestUpdateBotMetadataByBotId(t *testing.T) {
	bot := &Bot{}
	bot.CreateBot("test-bot-id", 1)

	fields := map[string]interface{}{
		"RecordingStarted": "2022-01-01T00:00:00Z",
		"MediaURL":         "http://example.com/media",
		"TranscriptionURL": "http://example.com/transcription",
	}

	UpdateBotByBotId("test-bot-id", fields)

	updatedBots, _ := GetBots(map[string]interface{}{"bot_id": "test-bot-id"})
	if len(updatedBots) == 0 {
		t.Errorf("No bot found with the given ID")
		return
	}
	updatedBot := updatedBots[0]
	if updatedBot.RecordingStarted.Format(time.RFC3339) != "2022-01-01T00:00:00Z" || updatedBot.MediaURL != "http://example.com/media" || updatedBot.TranscriptionURL != "http://example.com/transcription" {
		t.Errorf("Failed to update bot metadata")
	}

	UpdateBotByBotId("non-existent-bot-id", fields)
	// Check the logs for the error message
}

func TestGetMeetingFromBotId(t *testing.T) {
	bot := &Bot{}
	bot.CreateBot("test-bot-id", 1)

	_, err := GetMeetingFromBotId("test-bot-id")
	if err != nil {
		t.Errorf("Failed to get meeting from bot ID: %v", err)
	}

	_, err = GetMeetingFromBotId("non-existent-bot-id")
	if err == nil {
		t.Errorf("Expected error when getting meeting from non-existent bot ID, got nil")
	}
}

func TestCreateBotWithEmptyBotID(t *testing.T) {
	bot := &Bot{}
	_, err := bot.CreateBot("", 1)
	if err == nil {
		t.Errorf("Expected error when creating bot with empty ID, got nil")
	}
}

func TestCreateBotWithInvalidMeetingID(t *testing.T) {
	bot := &Bot{}
	_, err := bot.CreateBot("test-bot-id", 0)
	if err == nil {
		t.Errorf("Expected error when creating bot with invalid meeting ID, got nil")
	}
}

func TestUpdateBotMetadataByBotIdWithEmptyBotID(t *testing.T) {
	fields := map[string]interface{}{
		"RecordingStarted": "2022-01-01T00:00:00Z",
		"MediaURL":         "http://example.com/media",
		"TranscriptionURL": "http://example.com/transcription",
	}

	UpdateBotByBotId("", fields)
	// Check the logs for the error message
}

func TestUpdateBotMetadataByBotIdWithEmptyFields(t *testing.T) {
	fields := map[string]interface{}{}

	UpdateBotByBotId("test-bot-id", fields)
	// Check the logs for the error message
}

func TestGetMeetingFromBotIdWithEmptyBotID(t *testing.T) {
	_, err := GetMeetingFromBotId("")
	if err == nil {
		t.Errorf("Expected error when getting meeting from empty bot ID, got nil")
	}
}
*/
