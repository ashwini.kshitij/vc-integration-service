package db_models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"sync"
	"vc-integration-service/configs"
	"vc-integration-service/errors"
)

var (
	DB   *gorm.DB
	once sync.Once
)

func GetDB() *gorm.DB {
	once.Do(func() {
		config := configs.GetConfig().DB
		dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable",
			config.Host, config.User, config.Password, config.DBName)
		var err error
		DB, err = gorm.Open(config.Type, dsn)
		if err != nil {
			log.Fatalf("Error connecting to the database: %v", err)
		}

		DB.AutoMigrate(&Meeting{})
		DB.AutoMigrate(&Bot{})
		DB.AutoMigrate(&MeetingStatus{})

		//TODO: Move this to configs
		DB.DB().SetMaxIdleConns(10)
		DB.DB().SetMaxOpenConns(100)

		log.Println("Database connection successfully opened.")
	})
	return DB
}

// TransactionalOperation is a type for functions that will be executed in a transaction.
type TransactionalOperation func() error

// ExecuteInTransaction executes the given operation in a transaction.
func ExecuteInTransaction(operation TransactionalOperation) error {
	tx := GetDB().Begin()

	if tx.Error != nil {
		return errors.NewDBTransactionError("Begin", tx.Error)
	}

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			log.Println(errors.NewDBTransactionError("Rollback", fmt.Errorf("%v", r)))
		}
	}()

	if err := operation(); err != nil {
		tx.Rollback()
		return errors.NewDBTransactionError("Operation", err)
	}

	if err := tx.Commit().Error; err != nil {
		return errors.NewDBTransactionError("Commit", err)
	}
	return nil
}
