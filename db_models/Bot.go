package db_models

import (
	"gorm.io/gorm"
	"log"
	"time"
)

type Bot struct {
	gorm.Model
	BotID            string `gorm:"type:varchar(255);unique"`
	MeetingID        uint   `gorm:"foreignKey"`
	RecordingStarted time.Time
	MediaURL         string
	TranscriptionURL string
	Meeting          Meeting
}

func (b *Bot) CreateBot(BotID string, MeetingID uint) (*Bot, error) {
	b.BotID = BotID
	b.MeetingID = MeetingID

	db := GetDB().Create(b)
	if db.Error != nil {
		return nil, db.Error
	}
	return b, nil
}

func UpdateBotByBotId(botID string, fields map[string]interface{}) {
	result := GetDB().Model(&Bot{}).Where("bot_id = ?", botID).Updates(fields)
	if result.Error != nil {
		log.Printf("Error updating bot in database: %v", result.Error)
	}
}

func GetMeetingFromBotId(fields map[string]interface{}) (*Meeting, error) {
	bots, err := GetBots(fields)
	if err != nil {
		log.Printf("Error retrieving bot from database: %v", err)
		return nil, err
	}
	var meeting Meeting
	association := GetDB().Model(&bots[0]).Association("Meeting")
	if association.Error != nil {
		log.Printf("Error retrieving meeting association: %v", association.Error)
		return nil, association.Error
	}
	association.Find(&meeting)
	return &meeting, nil
}

func GetBots(fields map[string]interface{}) ([]Bot, error) {
	var bots []Bot
	db := GetDB().Model(&Bot{})
	for key, value := range fields {
		db = db.Where(key+" = ?", value)
	}
	result := db.Find(&bots)
	if result.Error != nil {
		log.Printf("Error retrieving bots from database: %v", result.Error)
		return nil, result.Error
	}
	return bots, nil
}
