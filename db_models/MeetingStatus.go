package db_models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type MeetingStatus struct {
	gorm.Model
	MeetingID       uint      `gorm:"type:int;not null;index;foreignkey:MeetingID;references:ID"`
	Status          string    `gorm:"type:varchar(100);not null"`
	StatusTimestamp time.Time `gorm:""`
	Meeting         Meeting   `gorm:"foreignkey:ID"`
}

func Create(meeting *Meeting) error {
	db := GetDB()
	result := db.Create(&MeetingStatus{
		MeetingID:       meeting.ID,
		Status:          meeting.RecordingStatus,
		StatusTimestamp: time.Now(),
	})
	if result.Error != nil {
		return result.Error
	}
	return nil
}
