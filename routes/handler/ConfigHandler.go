package handler

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"net/http"
	"vc-integration-service/configs"
)

// ConfigAPIHandler @Summary Get configuration
// @Description This API returns the configuration
// @Tags Configuration
// @Accept  json
// @Produce  json
// @Success 200 {object} map[string]interface{} "config: string"
// @Router /config [get]
func ConfigAPIHandler(c echo.Context) error {
	jsonData, err := json.Marshal(configs.GetConfig())
	if err != nil {
		// Return the error message with Internal Server Error status
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	} else {
		// Return the JSON response with OK status
		return c.JSONBlob(http.StatusOK, jsonData)
	}
}
