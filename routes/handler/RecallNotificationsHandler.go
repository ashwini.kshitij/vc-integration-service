package handler

import (
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"vc-integration-service/constants"
	"vc-integration-service/consumers"
	"vc-integration-service/dto/recall"
)

// BotStatusChangeHandler @Summary Get configuration
// @Description This API returns the configuration
// @Tags Configuration
// @Accept  json
// @Produce  json
// @Success 200 {object} map[string]interface{} "config: string"
// @Router /config [get]
func BotStatusChangeHandler(c echo.Context) error {
	event := new(recall.BotStatusChangeEvent)
	if err := c.Bind(event); err != nil {
		return err
	}

	err := constants.ValidateJSON(event)
	if err != nil {
		log.Printf("Validation failed: %v", err)
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	if event.Data.Status.Code == "done" {
		//TODO: Push to kafka to decouple processing from webhook response
		// Handle the event async in a separate goroutine
		go func() {
			consumers.HandleBotDone(event)
		}()
	}

	return c.JSON(http.StatusOK, map[string]string{"message": "Event received"})
}
