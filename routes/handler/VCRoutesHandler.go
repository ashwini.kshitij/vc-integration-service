package handler

import (
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"strconv"
	"vc-integration-service/db_models"
	"vc-integration-service/dto/recall/requests"
	"vc-integration-service/dto/vis"
	"vc-integration-service/service/factory"
)

// EnableRecordingHandler @Summary Enable recording for a meeting
// @Description This API enables the recording for a meeting
// @Tags Meetings
// @Accept  json
// @Produce  json
// @Param  requests.EnableRecordingRequest     true     "Enable Recording Request"
// @Success 200 {object} map[string]interface{} "meeting_id: uint"
// @Router /enable_recording [post]
func EnableRecordingHandler(c echo.Context) error {
	request := new(requests.EnableRecordingRequest)
	if err := c.Bind(request); err != nil {
		return err
	}

	validate := validator.New()
	err := validate.Struct(request)
	if err != nil {
		log.Printf("Validation failed: %v", err)
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	//print the request
	log.Printf("Request received: %v", request)

	meeting, err := (&db_models.Meeting{}).Create(
		request.MeetingURL,
		request.EventID,
		request.ScheduledStartTime,
		request.ScheduledEndTime,
	)
	if err != nil {
		log.Printf("Error persisting meeting in DB: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Error creating meeting")
	}

	platform, err := factory.GetExternalVCPlatform(request.ClientType)

	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}
	err = platform.EnableRecording(meeting)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	meeting, err = meeting.UpdateRecordingStatus("ENABLED")
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "Error updating meeting recording status")
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"meeting_id": meeting.ID,
	})
}

func GetMeetingStatusHandler(c echo.Context) error {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "Invalid meetings ID"})
	}

	// Fetch the meetings by Meeting.ID
	meetingFields := map[string]interface{}{"id": id}
	meetings, err := db_models.GetMeetings(meetingFields)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}

	// Fetch the bot by Meeting.ID
	botFields := map[string]interface{}{"meeting_id": id}
	bots, err := db_models.GetBots(botFields)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"error": err.Error()})
	}

	// Merge the meetings and bot into a MeetingAndBot struct
	meetingAndBot := vis.MeetingAndBot{
		Meeting: meetings[0],
		Bot:     bots[0],
	}

	return c.JSON(http.StatusOK, meetingAndBot)
}
