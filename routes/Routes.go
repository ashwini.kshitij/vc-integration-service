package routes

import (
	"github.com/labstack/echo/v4"
	"github.com/swaggo/echo-swagger"
	"vc-integration-service/routes/handler"
	"vc-integration-service/routes/middleware"
)

func InitRoutes() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger)
	e.GET("/config", handler.ConfigAPIHandler)

	e.POST("/recall/bot/status", middleware.Authentication(handler.BotStatusChangeHandler))
	e.POST("/enable_recording", handler.EnableRecordingHandler)
	e.GET("/meeting/:id", handler.GetMeetingStatusHandler)
	// Add Swagger endpoint
	e.GET("/swagger/*", echoSwagger.WrapHandler)
	return e
}
