package middleware

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"log"
	"time"
)

func Logger(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Log the request
		log.Printf("Received %s request for %s", c.Request().Method, c.Request().URL)

		// Start timer
		start := time.Now()

		// Process request
		err := next(c)

		// Calculate response time
		responseTime := time.Since(start)

		var status string
		if err != nil {
			status = err.Error()
		} else {
			status = fmt.Sprint(c.Response().Status)
		}
		log.Printf("Responded to %s request for %s with status code %s in %v", c.Request().Method, c.Request().URL, status, responseTime)
		return err
	}
}
