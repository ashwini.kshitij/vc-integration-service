package middleware

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"vc-integration-service/configs"
)

func Authentication(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		// Replace with your actual authentication logic
		authToken := c.Request().Header.Get("Authorization")
		if authToken != configs.GetConfig().Recall.BotStatusChangeEventWebhookToken {
			return echo.NewHTTPError(http.StatusUnauthorized, "Please provide valid credentials")
		}
		return next(c)
	}
}
