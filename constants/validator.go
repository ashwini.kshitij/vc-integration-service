package constants

import (
	"github.com/go-playground/validator/v10"
	"github.com/pkg/errors"
)

// ValidateJSON validates a struct using the validator package
func ValidateJSON(s interface{}) error {
	validate := validator.New()
	err := validate.Struct(s)
	if err != nil {
		return errors.Wrap(err, "validation failed")
	}
	return nil
}
