package constants

// API is a struct that contains the path and type of an API
type API struct {
	Path        string
	RequestType string
}

/*
APIs for the Recall service
*/
type RecallAPIStruct struct {
	CreateBot        API
	RetrieveBot      API
	DeleteBot        API
	GetSpeakerChange API
}

var RecallAPIs = RecallAPIStruct{
	CreateBot:        API{Path: "%s/api/v1/bot", RequestType: "POST"},
	RetrieveBot:      API{Path: "%s/api/v1/bot/%s/", RequestType: "GET"},
	DeleteBot:        API{Path: "%s/api/v1/bot/%s/", RequestType: "DELETE"},
	GetSpeakerChange: API{Path: "%s/api/v1/bot/%s/speaker_timeline/", RequestType: "GET"},
}
