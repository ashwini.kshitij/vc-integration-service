package constants

type VCTool uint
type ClientID uint

const (
	Zoom VCTool = iota + 1
	GoTo
	MSTeams
	GoogleMeet
)

var VCToolToClientID = map[VCTool]ClientID{
	Zoom:       1,
	GoTo:       2,
	MSTeams:    3,
	GoogleMeet: 4,
}

var ClientIDToVCTool = map[ClientID]VCTool{
	1: Zoom,
	2: GoTo,
	3: MSTeams,
	4: GoogleMeet,
}
