package recall

import (
	"errors"
	"sort"
	"time"
)

type StatusChange struct {
	Code      string    `json:"code"`
	CreatedAt time.Time `json:"created_at"`
}

type Recording struct {
	ID          string    `json:"id"`
	StartedAt   time.Time `json:"started_at"`
	CompletedAt time.Time `json:"completed_at"`
}

type BotResponse struct {
	ID                  string         `json:"id" validate:"required"`
	VideoURL            string         `json:"video_url"`
	MediaRetentionEnd   string         `json:"media_retention_end"`
	StatusChanges       []StatusChange `json:"status_changes"`
	MeetingMetadata     interface{}    `json:"meeting_metadata"`
	MeetingParticipants []interface{}  `json:"meeting_participants"`
	JoinAt              time.Time      `json:"join_at"`
	CalendarMeetings    []interface{}  `json:"calendar_meetings"`
	Recording           interface{}    `json:"recording"`
	Recordings          []Recording    `json:"recordings"`
}

func GetInCallRecordingTimeAsPerFirstOccurrence(botResponse *BotResponse) (time.Time, error) {
	/* Sort StatusChanges by CreatedAt.
	   In case of multiple pause/resume or start/stop, this will return start of first recording
	*/
	sort.Slice(botResponse.StatusChanges, func(i, j int) bool {
		return botResponse.StatusChanges[i].CreatedAt.Before(botResponse.StatusChanges[j].CreatedAt)
	})
	for _, statusChange := range botResponse.StatusChanges {
		if statusChange.Code == "in_call_recording" {
			return statusChange.CreatedAt, nil
		}
	}
	return time.Time{}, errors.New("IllegalState: No status with code 'in_call_recording' found")
}

// TODO: Might have to use call_in_recording -> call_not_in_recording. Done status is adding extra secs
func (botResponse *BotResponse) GetRecordingDuration() (float64, error) {
	if len(botResponse.Recordings) == 0 {
		return 0, errors.New("No recordings found")
	}

	// Sort Recordings by StartedAt
	sort.Slice(botResponse.Recordings, func(i, j int) bool {
		return botResponse.Recordings[i].StartedAt.Before(botResponse.Recordings[j].StartedAt)
	})

	// Get the first recording
	firstRecording := botResponse.Recordings[0]

	// Calculate and return the duration
	return firstRecording.CompletedAt.Sub(firstRecording.StartedAt).Seconds(), nil
}
