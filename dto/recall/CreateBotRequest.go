package recall

type CreateBotRequest struct {
	MeetingURL string `json:"meeting_url"`
	JoinAt     string `json:"join_at"`
	BotName    string `json:"bot_name"`
}
