package requests

import "time"

type EnableRecordingRequest struct {
	MeetingURL         string    `json:"meeting_url" validate:"required,url"`
	EventID            string    `json:"event_id" validate:"required"`
	ClientType         uint      `json:"client_type" validate:"required"`
	ScheduledStartTime time.Time `json:"scheduled_start_time" validate:"required"`
	ScheduledEndTime   time.Time `json:"scheduled_end_time" validate:"required"`
}
