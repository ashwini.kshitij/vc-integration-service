package recall

type BotStatusChangeEvent struct {
	Event string `json:"event" validate:"required"`
	Data  struct {
		BotID  string `json:"bot_id" validate:"required"`
		Status struct {
			Code        string `json:"code" validate:"required"`
			CreatedAt   string `json:"created_at"`
			SubCode     string `json:"sub_code"`
			Message     string `json:"message"`
			RecordingID string `json:"recording_id"`
		} `json:"status" validate:"required"`
	} `json:"data" validate:"required"`
}
