package recall

type SpeakerChange struct {
	Timestamp float64 `json:"timestamp"`
	UserID    int     `json:"user_id"`
	Name      string  `json:"name"`
}

type SpeakerChangeResponse struct {
	SpeakerChanges []SpeakerChange `json:"speaker_changes"`
}
