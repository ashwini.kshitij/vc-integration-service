package vis

import "vc-integration-service/db_models"

type MeetingAndBot struct {
	Meeting db_models.Meeting
	Bot     db_models.Bot
}
