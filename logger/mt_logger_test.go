package logger

/*
import (
	"context"
	"gitlab.com/mindtickle/mt-go-logger/constants"
	"gitlab.com/mindtickle/mt-go-logger/util"
	"go.uber.org/zap/zapcore"
	"testing"
)

var ctx context.Context

func init() {
	ctx = context.Background()
}

func TestDefaultLogger(t *testing.T) {
	Logger.Info(ctx, "Hello!")
}

func TestLoggerWithDifferentConfig(t *testing.T) {
	Logger.UpdateConfig(&LoggerConfig{IncludeNilTagValues: true})
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	testCtx = context.WithValue(testCtx, constants.ParentSpanIdTag, "4545")
	testCtx = context.WithValue(testCtx, constants.TraceIdTag, "abc-123-xyz")
	Logger.Info(testCtx, "Hello!")
}

func TestLoggerWithLargeMessageForTrunc(t *testing.T) {
	Logger.UpdateConfig(&LoggerConfig{IncludeNilTagValues: false, MaxLogSize: 20, TruncateMsg: true})
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	Logger.Info(testCtx, "Hello! Creating a larger than required length message to see if it gets truncated.")
}

func TestLoggerWithLargeMessageForChunks(t *testing.T) {
	Logger.UpdateConfig(&LoggerConfig{IncludeNilTagValues: false, MaxLogSize: 20, TruncateMsg: false})
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	Logger.Info(testCtx, "Hello! Creating a larger than required length message to see if it gets logged in chunks.")
}

func TestLoggerWithDifferentLogLevel(t *testing.T) {
	Logger.UpdateConfig(&LoggerConfig{IncludeNilTagValues: false, LogLevel: LogLevelInfo})
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	testCtx = context.WithValue(testCtx, constants.TraceIdTag, "abc-123-xyz")
	Logger.Debug(testCtx, "This should not be logged!")
	Logger.Info(testCtx, "Hello!")
}

func TestLoggerWithDifferentEncoder(t *testing.T) {
	Logger.UpdateConfig(&LoggerConfig{IncludeNilTagValues: false, Encoder: util.GetDefaultConsoleEncoder()})
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	testCtx = context.WithValue(testCtx, constants.TraceIdTag, "abc-123-xyz")
	Logger.Info(testCtx, "Hello!")
}

func TestLoggerWithDifferentWriteSyncerAndCallerSkip(t *testing.T) {
	zapWriteSyncers := util.GetDefaultWriteSyncers()
	zapWriteSyncers = append(zapWriteSyncers, zapcore.AddSync(&lumberjack.Logger{
		Filename:   "/logs/log.txt",
		MaxSize:    50,
		MaxBackups: 1,
		MaxAge:     1,
	}))
	Logger.UpdateConfig(&LoggerConfig{WriteSyncers: zapWriteSyncers, CallerSkip: 2})
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	testCtx = context.WithValue(testCtx, constants.TraceIdTag, "abc-123-xyz")
	Logger.Info(testCtx, "Hello!")
}

func TestLoggerWithApplicationLevelTags(t *testing.T) {
	externalTags := []string{"orgId", "requestType"}
	Logger.UpdateConfig(&LoggerConfig{}).AddTags(externalTags)
	testCtx := context.WithValue(ctx, "orgId", "1234")
	testCtx = context.WithValue(testCtx, "requestType", "INTERNAL")
	testCtx = context.WithValue(testCtx, constants.TraceIdTag, "abc-123-xyz")
	Logger.Info(testCtx, "Hello!")
}

func TestLoggerByRemovingTags(t *testing.T) {
	testCtx := context.WithValue(ctx, constants.RequestIdTag, "1234")
	testCtx = context.WithValue(testCtx, constants.ParentSpanIdTag, "4545")
	Logger.RemoveTag(constants.ParentSpanIdTag)
	Logger.Info(testCtx, "Hello!")
}

func TestLoggerWithUseCaseLevelTags(t *testing.T) {
	Logger.WithTag("use-case-key", "use-case-value").Info(ctx, "Hello!")
	Logger.Info(ctx, "Hello!")
}

func TestCreationOfLocalLoggers(t *testing.T) {
	logger1 := NewLogger().AddTag("test1-id").WithTag("processor", "P-1")
	logger2 := NewLogger().AddTag("test2-id").WithTag("processor", "P-2")
	testCtx := context.WithValue(ctx, "test1-id", "123")
	testCtx = context.WithValue(testCtx, "test2-id", "456")
	logger1.Info(testCtx, "Hello!")
	logger2.Info(testCtx, "Hello!")
	Logger.Info(testCtx, "Hello!")
}

func TestNilContext(t *testing.T) {
	Logger.Info(nil, "Hello!")
}
*/
