package errors

import (
	"fmt"
)

// DBTransactionError is a type for database transaction errors.
type DBTransactionError struct {
	Operation string
	Err       error
}

// Error implements the error interface.
func (e *DBTransactionError) Error() string {
	return fmt.Sprintf("Error in database transaction operation %s: %v", e.Operation, e.Err)
}

// NewDBTransactionError creates a new database transaction error.
func NewDBTransactionError(operation string, err error) error {
	return &DBTransactionError{
		Operation: operation,
		Err:       err,
	}
}
