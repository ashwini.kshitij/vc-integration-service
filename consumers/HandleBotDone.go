package consumers

import (
	"log"
	"vc-integration-service/db_models"
	"vc-integration-service/dto/recall"
	"vc-integration-service/scripts"
	"vc-integration-service/sdk"
)

// TODO: Push the botResponse and speaker changes to a document store like MongoDB
func HandleBotDone(event *recall.BotStatusChangeEvent) {
	recallClient := sdk.RecallClient()
	botResponse, err := recallClient.RetrieveBot(event.Data.BotID)
	if err != nil {
		log.Printf("Error retrieving botResponse: %v", err)
	}

	recordingStarted, err := recall.GetInCallRecordingTimeAsPerFirstOccurrence(&botResponse)
	if err != nil {
		log.Printf("Error getting in-call recording time: %v", err)
		return
	}

	db_models.UpdateBotByBotId(botResponse.ID, map[string]interface{}{
		"MediaURL":         botResponse.VideoURL,
		"RecordingStarted": recordingStarted,
	})

	/*speakerChangeResponse, err := recallClient.GetSpeakerChange(botResponse.ID)
	if err != nil {
		fmt.Printf("Error retrieving speaker change: %v", err)
		return
	}*/

	persistedBots, err2 := db_models.GetBots(map[string]interface{}{"bot_id": botResponse.ID})
	if err2 != nil {
		log.Printf("Error retrieving bot from database: %v", err2)
		return
	} else if len(persistedBots) == 0 {
		log.Printf("Bot not found in database: %v", botResponse.ID)
		return
	}
	persistedMeetings, err3 := db_models.GetMeetings(map[string]interface{}{"id": persistedBots[0].MeetingID})
	if err3 != nil {
		log.Printf("Error retrieving meeting from database: %v", err3)
		return
	} else if len(persistedMeetings) == 0 {
		log.Printf("Meeting not found in database: %v", persistedBots[0].MeetingID)
		return
	}

	persistedMeeting := persistedMeetings[0]
	persistedBot := persistedBots[0]

	duration, err4 := botResponse.GetRecordingDuration()
	if err4 != nil {
		log.Printf("Error getting recording duration: %v", err)
		return
	}

	log.Printf("Starting call migration for event id: %v", persistedMeetings[0].EventID)
	scripts.MigrateToCallAI(persistedMeeting.ScheduledStartTime, persistedBot.BotID, duration, botResponse.VideoURL)
	log.Printf("Postprocessing executed successfully: %v", botResponse.ID)
}
